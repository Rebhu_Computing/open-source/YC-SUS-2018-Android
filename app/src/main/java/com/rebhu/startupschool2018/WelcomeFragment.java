package com.rebhu.startupschool2018;


import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.evernote.android.job.JobRequest;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class WelcomeFragment extends Fragment implements View.OnClickListener {

    private CheckBox checkBoxSkip, checkBoxPrivacyPolicy;
    private EditText editTextEmailWelcomeFragment;
    private SharedPreferences.Editor editor;
    private API api;

    public WelcomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_welcome, container, false);

        ImageView imageViewSUSLogo = view.findViewById(R.id.imageViewSUSWelcomeFragment);
        ImageView imageView8hootLogoWelcomeFragment =
                view.findViewById(R.id.imageView8hootLogoWelcomeFragment);

        TextView textViewPrivacyPolicyWelcomeFragment =
                view.findViewById(R.id.textViewPrivacyPolicyWelcomeFragment);

        checkBoxSkip = view.findViewById(R.id.checkBoxSkipWelcomeFragment);
        checkBoxPrivacyPolicy = view.findViewById(R.id.checkBoxPrivacyWelcomeFragment);

        editTextEmailWelcomeFragment = view.findViewById(R.id.editTextEmailWelcomeFragment);

        Button buttonEarlyAccessWelcomeFragment =
                view.findViewById(R.id.buttonEarlyAccessWelcomeFragment);

        imageViewSUSLogo.setOnClickListener(this);
        imageView8hootLogoWelcomeFragment.setOnClickListener(this);

        textViewPrivacyPolicyWelcomeFragment.setOnClickListener(this);

        buttonEarlyAccessWelcomeFragment.setOnClickListener(this);

        api = HTTPClient.getRetrofitClient().create(API.class);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        editor = preferences.edit();

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.textViewPrivacyPolicyWelcomeFragment:
                String urlPrivacyPolicy = "https://8hoot.com/privacy-policy.html";
                Intent intentPrivacyPolicy = new Intent(Intent.ACTION_VIEW);
                intentPrivacyPolicy.setData(Uri.parse(urlPrivacyPolicy));
                startActivity(intentPrivacyPolicy);
                break;
            case R.id.imageViewSUSWelcomeFragment:
                //todo: open startupschool.org and check if checkboxskip is selected
                if (checkBoxSkip.isChecked()) {
                    editor.putBoolean(Constants.SHOULD_SKIP_WELCOME, true).apply();
                }
                proceedToWebViewActivity();
                break;
            case R.id.imageView8hootLogoWelcomeFragment:
                String url = "https://8hoot.com/";
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                startActivity(intent);
                break;
            case R.id.buttonEarlyAccessWelcomeFragment:
                if (checkBoxSkip.isChecked()) {
                    editor.putBoolean(Constants.SHOULD_SKIP_WELCOME, true).apply();
                }
                String email = editTextEmailWelcomeFragment.getText().toString().trim();
                if (checkBoxPrivacyPolicy.isChecked() && !email.isEmpty()) {
                    editor.putString(Constants.EMAIL, email).apply();
                    api.registerUser(email, "Android")
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribe(getResponseSingleObserver());
                } else {
                    Toast.makeText(getContext(), R.string.please_read_privacy, Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private SingleObserver<Response<RegRespModel>> getResponseSingleObserver() {

        return new SingleObserver<Response<RegRespModel>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(Response<RegRespModel> regResponse) {
                if (regResponse.isSuccessful()) {
                    RegRespModel regRespModel = regResponse.body();
                    switch (regRespModel.getStatus()) {
                        case "DBConnFail":
                            Toast.makeText(getContext(), R.string.error, Toast.LENGTH_LONG).show();
                            proceedToWebViewActivity();
                            break;
                        case "conn-fail":
                            Toast.makeText(getContext(), R.string.error, Toast.LENGTH_LONG).show();
                            proceedToWebViewActivity();
                            break;
                        case "duplicate":
                            Toast.makeText(getContext(), R.string.duplicate, Toast.LENGTH_LONG).show();
                            break;
                        case "registered":
                            Toast.makeText(getContext(), R.string.registered, Toast.LENGTH_LONG).show();
                            editor.putBoolean(Constants.IS_REGISTERED, true);
                            editor.putBoolean(Constants.SHOULD_SKIP_WELCOME, true);
                            editor.remove(Constants.EMAIL);
                            editor.apply();

                            proceedToWebViewActivity();
                            break;
                        default:
                            Toast.makeText(getContext(), R.string.error, Toast.LENGTH_LONG).show();
                            proceedToWebViewActivity();
                            break;
                    }
                } else {
                    Toast.makeText(getContext(), R.string.error, Toast.LENGTH_LONG).show();
                    proceedToWebViewActivity();
                }
            }

            @Override
            public void onError(Throwable e) {
                Toast.makeText(getContext(), R.string.error, Toast.LENGTH_LONG).show();
                proceedToWebViewActivity();
            }
        };
    }

    private void proceedToWebViewActivity() {
        new JobRequest.Builder(Constants.REG_JOB_TAG)
                .setExecutionWindow(30_000L, 40_000L)
                .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                .build()
                .schedule();

        startActivity(new Intent(getActivity(), WebViewActivity.class));
    }
}
