# YC-SUS-2018-Android

An open source project to give YC Startup School 2018 users an easy access to the resources on their Android mobile devices.

All the resources of 8hoot, Y Combinator etc. are propery of respective brands. The use of these should be done while respecting the brand. Also, if there is any objection with the use of any of the resources or content then please file an issue and have `[OBJECTIONABLE RESOURCE]` at the start of issue title or connect with us at chintan@rebhu.com and we will be proactive in finding a resolution to the issue.

The code for the application is available as **Apache License 2.0** feel free to contribute or convert your mobile-optimized web app to mobile app without writing a single line of native code. You are expected to respect the trademarks of respective brands.